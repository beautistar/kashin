/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 5.7.22-0ubuntu0.16.04.1 : Database - peek
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`peek` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `peek`;

/*Table structure for table `ci_block` */

DROP TABLE IF EXISTS `ci_block`;

CREATE TABLE `ci_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_block` */

insert  into `ci_block`(`id`,`user_id`,`target_id`,`status`,`created_at`) values (1,12,13,'unblocked','2018-05-03 02:05:55'),(2,26,28,'unblocked','2018-05-04 10:05:11'),(3,26,15,'blocked','2018-05-04 10:05:26'),(4,26,27,'unblocked','2018-05-04 02:05:30'),(5,26,27,'unblocked','2018-05-04 06:05:42'),(6,26,27,'unblocked','2018-05-04 08:05:51'),(7,27,28,'unblocked','2018-05-05 09:05:09'),(8,27,26,'unblocked','2018-05-05 08:05:04'),(9,27,28,'unblocked','2018-05-06 10:05:52'),(10,26,30,'unblocked','2018-05-18 02:05:27');

/*Table structure for table `ci_contact` */

DROP TABLE IF EXISTS `ci_contact`;

CREATE TABLE `ci_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `is_favorite` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_contact` */

insert  into `ci_contact`(`id`,`user_id`,`target_id`,`status`,`is_favorite`,`updated_at`) values (1,13,12,'Accepted','No','2018-04-29 02:04:19'),(2,12,13,'Accepted','No','2018-05-18 12:05:12'),(3,28,27,'Accepted','Yes','2018-04-30 12:04:21'),(4,27,28,'Accepted','Yes','2018-04-30 12:04:21'),(5,26,27,'Accepted','Yes','2018-04-30 01:04:47'),(6,27,26,'Accepted','Yes','2018-04-30 01:04:47'),(7,26,28,'Accepted','Yes','2018-05-02 04:05:13'),(8,26,22,'Accepted','Yes','2018-05-04 06:05:53'),(9,28,26,'Accepted','Yes','2018-05-02 04:05:13'),(10,22,26,'Accepted','No','2018-05-04 06:05:53'),(11,1,27,'Invited','No','2018-05-05 06:05:35'),(13,33,32,'Invited','No','2018-05-06 04:05:08'),(14,28,32,'Accepted','Yes','2018-05-06 05:05:20'),(15,32,28,'Accepted','Yes','2018-05-06 05:05:20'),(20,19,29,'Invited','No','2018-05-07 02:05:45'),(21,12,29,'Accepted','Yes','2018-05-07 02:05:25'),(22,32,29,'Accepted','No','2018-05-07 08:05:28'),(23,29,12,'Accepted','Yes','2018-05-07 06:05:29'),(24,27,34,'Accepted','Yes','2018-05-09 02:05:12'),(25,34,27,'Accepted','Yes','2018-05-07 06:05:33'),(26,29,32,'Accepted','Yes','2018-05-07 08:05:52'),(27,29,13,'Accepted','Yes','2018-05-07 06:05:31'),(28,13,29,'Accepted','No','2018-05-07 09:05:09'),(29,31,28,'Invited','No','2018-05-08 08:05:36'),(30,33,28,'Invited','No','2018-05-08 08:05:37'),(31,34,28,'Accepted','No','2018-05-18 05:05:34'),(32,29,28,'Accepted','Yes','2018-05-10 01:05:41'),(33,30,28,'Accepted','Yes','2018-05-18 03:05:16'),(34,28,28,'Invited','No','2018-05-08 08:05:41'),(35,28,35,'Accepted','Yes','2018-05-09 04:05:20'),(36,35,28,'Accepted','No','2018-05-09 03:05:44'),(37,28,34,'Accepted','Yes','2018-05-09 08:05:16'),(38,27,38,'Accepted','No','2018-05-09 07:05:06'),(39,38,27,'Accepted','Yes','2018-05-09 07:05:23'),(40,28,29,'Accepted','Yes','2018-05-12 12:05:35'),(41,30,26,'Accepted','Yes','2018-05-18 01:05:54'),(42,29,26,'Accepted','No','2018-05-18 05:05:28'),(43,26,30,'Accepted','No','2018-05-18 01:05:06'),(44,28,30,'Accepted','No','2018-05-18 01:05:07'),(45,26,29,'Accepted','No','2018-05-18 05:05:28');

/*Table structure for table `ci_history` */

DROP TABLE IF EXISTS `ci_history`;

CREATE TABLE `ci_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `requested_at` datetime NOT NULL,
  `accepted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_history` */

insert  into `ci_history`(`id`,`user_id`,`target_id`,`status`,`requested_at`,`accepted_at`) values (1,12,13,'Acceptted','2018-04-25 01:04:32','2018-04-29 10:04:11'),(2,12,13,'Acceptted','2018-04-29 10:04:41','2018-04-29 10:04:11'),(3,14,13,'Requested','2018-04-29 10:04:01','0000-00-00 00:00:00'),(4,27,28,'Acceptted','2018-04-30 12:04:39','2018-04-30 12:04:48');

/*Table structure for table `ci_users` */

DROP TABLE IF EXISTS `ci_users`;

CREATE TABLE `ci_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT 'or facebook_id',
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `photo_url` varchar(255) NOT NULL,
  `token` varchar(512) NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '1',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

/*Data for the table `ci_users` */

insert  into `ci_users`(`id`,`username`,`firstname`,`lastname`,`email`,`mobile_no`,`password`,`photo_url`,`token`,`role`,`is_active`,`is_admin`,`last_ip`,`created_at`,`updated_at`) values (1,'Admin','admin','admin','admin@admin.com','123456','$2y$10$8T0rYWBgxjO7fA8pS2L2Fe7CMnjx17pxPOHSRIddI1cjZJs6XDFAq','','',1,1,1,'','2018-04-22 20:28:36','2018-04-22 20:28:41'),(12,'test1','','','test1@user.com','123456789','$2y$10$5V7HlNT1cgHm1GyTD6Oc6.We/xu9.GXU4zXQSVuSBhhXr8vgCo3b.','http://13.126.68.181/uploadfiles/2018/05/30_15254452418.png','sfsdfsadfsdafsdfsdsfd',1,1,0,'','2018-04-25 09:04:50','2018-04-25 12:04:00'),(13,'test2','','','test2@user.com','1234567890','$2y$10$fDowhTJZjEoVfAyKumPrlOxfjLf0Mgj2rVUg6QeYoFFt5Ublk5q0m','http://13.126.68.181/uploadfiles/2018/05/30_15254452418.png','',1,1,0,'','2018-04-25 12:04:57','2018-04-26 08:04:11'),(14,'test3','','','test3@user.com','123456789','$2y$10$BuqhZGqmv7GHrsX4UAViCupKaeGvlJLkR7hCidp0N1VqSBPjlCfuu','','',1,1,0,'','2018-04-26 07:04:46','2018-04-26 07:04:46'),(15,'waheed','','','waheed@user.com','1234567896','$2y$10$OKep37mb3N6Xw7bPGHZcheNC5N4/FCuu9h7Jv0jvMlcpb7CRtCFFm','http://13.126.68.181/uploadfiles/2018/05/30_15254452418.png','',1,1,0,'','2018-04-26 08:04:14','2018-04-29 05:04:08'),(16,'waheed','','','m@gmail.com','1234567859','$2y$10$CcjwjUmy5dVDuuVCyXW2AOcKN5rThaA4v4XZ.F5bu2r3focnGhWNS','','',1,1,0,'','2018-04-27 12:04:31','2018-04-27 12:04:31'),(17,'test4','','','test4@user.com','123456789','$2y$10$wnLr5uiVNV6FWF9.1WVlAuv6iCKY4ygNMjER.CG14rGL9hWJjC4O6','','',1,1,0,'','2018-04-27 12:04:32','2018-04-27 12:04:32'),(18,'aqeel','','','aqeel@gmail.com','+923034486786','$2y$10$phjK5A5FRjqB9AzxbNb2gu/Nkfjm.U00dwCbLyLv1OWwNqQEXY3FC','','',1,1,0,'','2018-04-27 05:04:37','2018-04-27 05:04:37'),(19,'test10','','','test10@user.com','123456789','$2y$10$AjUCsURRAuYHQlJul6T.AeGxKyuaHk7aUMCOBF.9EVScJg9twjKVC','','',1,1,0,'','2018-04-28 06:04:50','2018-04-28 06:04:50'),(20,'test5','','','test5@user.com','123456789','$2y$10$tXIzsjxDKUPWwptuCq2S2OYb3BqVGRPbQ/n7Ha6vfJwER9OZXC.Yq','','',1,1,0,'','2018-04-29 07:04:33','2018-04-29 07:04:33'),(21,'','','','Alif','030344','$2y$10$KhFRGM9JwQ0tK760VI1FVe/TgCR2b/jzUUdsDx1SxmbwrHVXI3hZa','','',1,1,0,'','2018-04-29 07:04:19','2018-04-29 07:04:19'),(22,'waheed','','','m.waheedsabir@outlook.com','03034486786','$2y$10$8tAmXlJpVNMqT8AEYrcZC.V8MHyPndxwPG66mE2oxSjeGqFtzqVP.','','',1,1,0,'','2018-04-29 07:04:49','2018-04-29 07:04:49'),(23,'aqeell','','','aqeel\r\nl@gmail.com','+923034486786','$2y$10$1wxCfti0ErffqD2HHzIQR.cqc0b1jUZxm5mvrJMg/6wX55wjSol2.','','',1,1,0,'','2018-04-29 08:04:42','2018-04-29 08:04:42'),(24,'waheed','','','m.waheed@gmail.com','+923034486786','$2y$10$2RsYHdQwGQfNFaD3b9.eR.3HJAMJwTaDsreyH3rxIJsALhMzLE/tS','','',1,1,0,'','2018-04-29 08:04:40','2018-04-29 08:04:40'),(25,'waheed','','','m.wa@gmail.com','123','$2y$10$C3f28goaUTvtJ0tSGfuVkOBXCqlxz3DA1.6o1CqNqt8GaXsq5KOUm','','',1,1,0,'','2018-04-29 08:04:20','2018-04-29 08:04:20'),(26,'waheedsabir','','','m.waheedsabir@yahoo.com','03034486786','$2y$10$MS2d5BYLkLVemRQTJUnote522ELlHy.kBWq5dLK7P7h3EUtIqZK0e','http://13.126.68.181/uploadfiles/2018/05/30_15254452418.png','',1,1,0,'','2018-04-29 08:04:28','2018-04-29 10:04:48'),(27,'a','','','a@a.com','03037616341','$2y$10$ZbFOjNVGZPVkK2/f1U.ofOFKDns8lK9GYPgwbyPSFi8.jrtvjOWFm','http://13.126.68.181/uploadfiles/2018/05/27_15258913009.jpg','',1,1,0,'','2018-04-30 11:04:52','2018-05-09 06:05:40'),(28,'b','','','b@b.com','03037616341','$2y$10$lazznIP.CLyDbKD2L30qNe.4.AVfoDXuNoN0sC3D7HpjPBUL4iCM2','','',1,1,0,'','2018-04-30 11:04:19','2018-04-30 11:04:19'),(29,'sss','','','iPhone1@user.com','1234567890','$2y$10$GaRpJ5u/uEZAC6R0vsspaeXGAJL28r3J.ON6EJK/nc3fj9DEaK9ge','http://13.126.68.181/uploadfiles/2018/04/12_15246599515.png','',1,1,0,'','2018-05-04 01:05:48','2018-05-04 01:05:48'),(30,'iPhone2','','','iPhone2@user.com','2222222222','$2y$10$k1ghTWJXBZol4gwAjtlpn.b9sqqRtOfZ.BXEmryQ6.N6ySOC7ODpe','http://13.126.68.181/uploadfiles/2018/05/30_15254452418.png','',1,1,0,'','2018-05-04 02:05:19','2018-05-04 02:05:21'),(31,'keyboard','','','keyboard@user.com','1234567890','$2y$10$FMsWzit9RdMdsVebgJQjvuHGamXpBf1mhPwH1DeKh915zG3SNZHsO','http://13.126.68.181/uploadfiles/2018/05/31_15254526316.png','',1,1,0,'','2018-05-04 04:05:28','2018-05-04 04:05:31'),(32,'1','','','1@gmail.com','03034486786','$2y$10$Ttd/azhbkCJjt2DR8tmv3.EqekSHlfEVeFxYNzWMjiF9jbQekNFUS','','',1,1,0,'','2018-05-06 04:05:03','2018-05-06 04:05:03'),(33,'2','','','2@gmail.com','03034486786','$2y$10$DIWDPzuJmOtt0o9fyNhgM.Ca2nDv2Ha6UmddQBB0BXYXMA/vZ25MK','','',1,1,0,'','2018-05-06 04:05:15','2018-05-06 04:05:15'),(34,'c','','','c@c.com','971828997','$2y$10$GzQi6gt/fP/cg8qs0P3NHu0hlMzWdKbIbs0vA.PNlnKSgHV70IUgu','','',1,1,0,'','2018-05-07 04:05:19','2018-05-07 04:05:19'),(35,'bilal','','','bilal@gmail.com','123','$2y$10$JmNArQ9bUPmAc9pzN7Vkjutp/70cAgQ7asj.8jsz.HxILfRuBzwsa','','',1,1,0,'','2018-05-09 03:05:52','2018-05-09 03:05:52'),(36,'test1','','','test111@user.com','123456789','$2y$10$WNSbnvuJahwUVCcpDSXt9OCBEdrZIeMVR5dANaKi9lxXed887256G','','',1,1,0,'','2018-05-09 05:05:48','2018-05-09 05:05:48'),(37,'d','','','d@d.com','03037616341','$2y$10$HCmZqLEqkh4hj2L7hHSII.rCR.IuQcHOKwPF65chOKlZQR4wqS13m','http://13.126.68.181/uploadfiles/2018/05/37_15258914444.jpg','',1,1,0,'','2018-05-09 06:05:03','2018-05-09 06:05:04'),(38,'e','','','e@e.com','4763844','$2y$10$jjAjsdHqMdAHqvC1fFSc2e0/z9JVjUrmx3P32t.1kOjhwOAJ8U2.S','http://13.126.68.181/uploadfiles/2018/05/38_15258918873.jpg','',1,1,0,'','2018-05-09 06:05:26','2018-05-09 06:05:27');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
