<?php
    class Transaction_model extends CI_Model{

        public function add_user($data){
            $this->db->insert('ci_users', $data);
            return true;
        }

        public function get_all_transactions(){
            
            $this->db->select('ci_order.*, ci_photo.photo_url');
            $this->db->from('ci_order');
            $this->db->join('ci_photo','ci_photo.photo_id=ci_order.photo_id');
            //$this->db->join('ci_finished', 'ci_order.photo_id=ci_finished.origin_id');
            $query=$this->db->get();

            return $result = $query->result_array();
        }
        
        public function get_photourl_by_id($photo_id){
            
            return $this->db->where('photo_id', $photo_id)->get('ci_photo')->row()->photo_url;
        }
        
        public function get_finished_photourl_by_id($photo_id) {
            return $this->db->where('photo_id', $photo_id)->get('ci_order')->row()->finished_file;
        }
        
        public function finish_return($data) {
            
            $this->db->insert('ci_finished', $data);
            return true;             
        }
        
        public function update_order($id, $data) {
            
            $this->db->where('id', $id);
            $this->db->update('ci_order', $data);
            return true;
            
        }
        
        public function get_all_users_for_csv(){
            $this->db->where('is_admin', 0);
            $this->db->select('id, username, firstname, lastname, email, mobile_no, created_at');
            $this->db->from('ci_users');
            $query = $this->db->get();
            return $result = $query->result_array();
        }
        

        public function get_user_by_id($id){
            $query = $this->db->get_where('ci_users', array('id' => $id));
            return $result = $query->row_array();
        }

        public function edit_user($data, $id){
            $this->db->where('id', $id);
            $this->db->update('ci_users', $data);
            return true;
        }

    }

?>