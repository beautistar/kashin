<?php
  
  class Api_model extends CI_Model {
      
      function exist_user_email($email) {
          
          $query = $this->db->get_where('tb_user', array('email' => $email));
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
      }
      
      function add_user($data) {
          
          $this->db->insert('tb_user', $data);
          return $this->db->insert_id();
      }
      
      public function login($data){
            $query = $this->db->get_where('tb_user', array('email' => $data['email']));
            if ($query->num_rows() == 0){
                return false;
            }
            else{
                //Compare the password attempt with the password we have stored.
                $result = $query->row_array();
                $validPassword = password_verify($data['password'], $result['password']);
                if($validPassword){
                    return $result = $query->row_array();
                }                
            }
      }
      
      function addNewStore($data) {
          
          $this->db->insert('tb_store', $data);
          return $this->db->insert_id();           
      }
      
      function addNewProduct($data) {
          
          $this->db->insert('tb_product', $data);
          return $this->db->insert_id();           
      }
      
      function addNewAwardByTeacher($data) {
          
          $this->db->insert('tb_award', $data);
          return $this->db->insert_id();           
      }
      
      function getStoreListByOrganization($user_id) {
          
          $query = $this->db->get_where('tb_store', array('user_id' => $user_id));
          return $query->result_array();
      }
      
      function getProductList($store_id) {
          
          $query = $this->db->get_where('tb_product', array('store_id' => $store_id));
          return $query->result_array();
      }
      
      function getAllTeachers() {
          
          $query = $this->db->get_where('tb_user', array('user_type' => '222'));
          return $query->result_array();
      }
      
      function getAllStudents() {
          
          $query = $this->db->get_where('tb_user', array('user_type' => '333'));
          return $query->result_array();
      }
      
      function getTeacherByCreatorId($creator_id) {
          
          $query = $this->db->get_where('tb_user', array('user_type' => '222', 'creator_id' => $creator_id));
          return $query->result_array();
      }
      
      function getStudentByCreatorId($creator_id) {
          
          $query = $this->db->get_where('tb_user', array('user_type' => '333', 'creator_id' => $creator_id));
          return $query->result_array();
      }
      
      function upload_photo($user_id, $data) {
        
          $this->db->where('id', $user_id); 
          $this->db->update('tb_user', $data);
          return true;
      }
      
      function addQrcode($user_id, $qr_code) {
          
          $this->db->where('id', $user_id);
          $this->db->set('qr_code', $qr_code);
          $this->db->update('tb_user');
      }
      
      function getStudentByQrCode($qr_code) {
          
          $query = $this->db->get_where('tb_user', array('user_type' => '333', 'qr_code' => $qr_code));
          return $query->row_array();
      } 
      
      function exist_transaction_id($transaction_id) {
          
          $query = $this->db->get_where('tb_statement', array('transaction_id' => $transaction_id));
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
          
      }     
      
      function sendMoney($data) {
          
          $this->db->insert('tb_statement', $data);
          return $this->db->insert_id();           
      }
      
      function getStatement($user_id) {
          
          $this->db->where('from', $user_id);
          $this->db->or_where('to', $user_id);
          return $this->db->get('tb_statement')->result_array();
      }
      
      function editStore($id, $data) {
          $this->db->where('id', $id);
          $this->db->update('tb_store', $data);
      }
      
      function editProduct($id, $data) {
          
          $this->db->where('id', $id);
          $this->db->update('tb_product', $data);
          
      }
      
      function getFundsList($user_id) {
          
          $this->db->where('from', $user_id);
          $this->db->or_where('to', $user_id);
          return $this->db->get('tb_statement')->result_array();
      }
      function exist_class($name) {
          
          $query = $this->db->get_where('tb_class', array('name' => $name));
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
      }
      
      function addClass($data) {
          
          $this->db->insert('tb_class', $data);
          return $this->db->insert_id(); 
          
      }
      
      function getClassList($user_id) {
          
          return $this->db->where('organization_id', $user_id)->get('tb_class')->result_array();
      }
      
      function editClass($id, $data) {
          
          $this->db->where('id', $id);
          $this->db->update('tb_class', $data);
      }
      
      function deleteClass($id) {
          
          $this->db->where('id', $id);
          $this->db->delete('tb_class');
          
          $this->db->where('class_id', $id);
          $this->db->delete('tb_class_student');
          
          $this->db->where('class_id', $id);
          $this->db->delete('tb_class_teacher');
      }
      
      function getAllProductList() {
          
          $query = $this->db->get('tb_product');
          return $query->result_array();
      }
      
      function addCheckout($data) {
          $this->db->insert('tb_checkout', $data);
      }
      
      function getCheckoutList($student_id, $organization_id) {
          
          return $this->db->select('t1.product_id, t1.created_at, t2.name, t2.price')
                     ->from('tb_checkout as t1')
                     ->where('t1.student_id', $student_id)
                     ->where('t1.organization_id', $organization_id)
                     ->join('tb_product as t2', 't1.product_id = t2.id', 'LEFT')
                     ->get()
                     ->result_array();
      }
      
      function addTeacherToClass($data) {
          
          $query = $this->db->get_where('tb_class_teacher', 
                                       array('class_id' => $data['class_id'], 
                                            "teacher_id" => $data['teacher_id']));
          if ($query->num_rows() == 0){
                $this->db->insert('tb_class_teacher', $data);
                return true;
          } else {
              return false;
          } 
      }
      
      function addStudentToClass($data) {
          
          $query = $this->db->get_where('tb_class_student', 
                                       array('class_id' => $data['class_id'], 
                                            "student_id" => $data['student_id']));
          if ($query->num_rows() == 0){
                $this->db->insert('tb_class_student', $data);
                return true;
          } else {
              return false;
          }
      }
      
      function getStudentClassRanking($class_id) {
          
          return $this->db->select('tb_user.*, SUM(tb_statement.amount) as amount')
                    ->from('tb_class_student')
                    ->where('tb_class_student.class_id', $class_id)
                    ->join('tb_statement', 'tb_statement.to = tb_class_student.student_id')
                    ->join('tb_user', 'tb_user.id = tb_statement.to')
                    ->group_by('tb_statement.to')
                    ->order_by('amount', 'DESC')
                    ->get()
                    ->result_array(); 
      }
      
      function getStudentClassRankingForTeacher($class_id) {
          
          return $this->db->select('tb_user.id as student_id,
                                    tb_user.name, 
                                    tb_user.qr_code, 
                                    SUM(tb_statement.amount) as amount')
                    ->from('tb_class_student')
                    ->where('tb_class_student.class_id', $class_id)
                    ->join('tb_statement', 'tb_statement.to = tb_class_student.student_id')
                    ->join('tb_user', 'tb_user.id = tb_statement.to')
                    ->group_by('tb_statement.to')
                    ->order_by('amount', 'DESC')
                    ->get()
                    ->result_array(); 
      }
      
      /*************
      * get class list from student id
      * check organization from the classes
      * @param mixed $user_id : student id
      */
      
//      function getStudentOrganizationList($user_id) {
//          
//          return $this->db->select('t1.name as organization_name, t3.organization_id,
//                                    t2.class_id, t3.name as class_name,
//                                    t3.description as class_description')
//                            ->from('tb_class_student as t2')
//                            ->where('t2.student_id', $user_id)
//                            ->join('tb_class as t3', 't3.id = t2.class_id')
//                            ->join('tb_user as t1', 't1.id = t3.organization_id')
//                            ->get()
//                            ->result_array();            
//          
//      } 
      
      function sendAward($data) {
          
          $query = $this->db->get_where('tb_send_award', $data);
          
          if ($query->num_rows() == 0) {
              $this->db->insert('tb_send_award', $data); 
          } 
      }
      
      function getAwardList($user_id, $organization_id) {
          
          return $this->db->select('t1.*')
                      ->from('tb_send_award as t2')
                      ->where('t2.student_id', $user_id)
                      ->where('t3.organization_id', $organization_id)
                      ->join('tb_award as t1', 't1.id = t2.award_id')
                      ->join('tb_class as t3', 't3.id = t2.class_id')
                      ->get()
                      ->result_array();
          
          //$query = $this->db->get_where('tb_award', array('student_id' => $user_id));
          //return $query->result_array();
      }
      
      function addStudentToOrganization($data) {
          
          $query = $this->db->get_where('tb_org_student', 
                                       array('organization_id' => $data['organization_id'], 
                                            "student_id" => $data['student_id']));
          if ($query->num_rows() == 0){
                $this->db->insert('tb_org_student', $data);
                return true;
          } else {
              return false;
          }
      }
      
      function addTeacherToOrganization($data) {
          
          $query = $this->db->get_where('tb_org_teacher', 
                                       array('organization_id' => $data['organization_id'], 
                                            "teacher_id" => $data['teacher_id']));
          if ($query->num_rows() == 0){
                $this->db->insert('tb_org_teacher', $data);
                return true;
          } else {
              return false;
          }
      }
      
      function setOrganizated($user_id) {
          
          $this->db->where('id', $user_id);
          $this->db->set('is_organizated', 1);
          $this->db->update('tb_user');
      }
      
      function getAllOrganizatedTeachers($organization_id) {
          
          return $this->db->select('t1.*')
                      ->from('tb_user as t1')                       
                      ->join('tb_org_teacher as t2', 't1.id = t2.teacher_id')                       
                      ->where('t1.user_type', 222)
                      ->where('t2.organization_id', $organization_id)
                      ->get()
                      ->result_array();
      }
      
      function getAllUnOrganizatedTeachers() { 
      
          $ids = array();         
          
          $ids_array = $this->db->select('t1.id as id')
                      ->from('tb_user as t1')                       
                      ->join('tb_org_teacher as t2', 't1.id = t2.teacher_id')                       
                      ->where('t1.user_type', 222)
                      //->where('t2.organization_id', $organization_id)
                      ->get()
                      ->result_array();
          
          if (count($ids_array) > 0) {
              foreach ($ids_array as $id) {
                  array_push($ids, $id['id']);              
              } 
                          
              return $this->db->select("*")
                       ->from('tb_user')
                       ->where_not_in('id', $ids)
                       ->where('user_type', 222)
                       ->get()
                       ->result_array();                
          } else {
              return array();
          }          
      }
      
      function getTeacherOrganizationList($user_id) {
          
          return $this->db->select('t1.name as organization_name, t3.organization_id,
                                    t2.class_id, t3.name as class_name, t2.class_id,
                                    t3.description as class_description')
                            ->from('tb_class_teacher as t2')
                            ->where('t2.teacher_id', $user_id)
                            ->join('tb_class as t3', 't3.id = t2.class_id')
                            ->join('tb_user as t1', 't1.id = t3.organization_id')
                            ->get()
                            ->result_array();            
          
      }
      
      function getStudentOrganizationList($user_id) {
          
          return $this->db->select('t1.name as organization_name, t3.organization_id,
                                    t2.class_id, t3.name as class_name, t2.class_id,
                                    t3.description as class_description')
                            ->from('tb_class_student as t2')
                            ->where('t2.student_id', $user_id)
                            ->join('tb_class as t3', 't3.id = t2.class_id')
                            ->join('tb_user as t1', 't1.id = t3.organization_id')
                            ->get()
                            ->result_array();            
          
      }
      
      function getAwardListByTeacher($user_id) { 
          
          $query = $this->db->get_where('tb_award', array('teacher_id' => $user_id));
          return $query->result_array();
      }
      
      function getAllStoreList() {
          
          $query = $this->db->get('tb_store');
          return $query->result_array();
      }
      
      function getOrganizationProductList($organization_id) {
          
          $query = $this->db->get_where('tb_product', array('user_id'=>$organization_id));
          return $query->result_array();
      }
      
      function getStoreProductList($store_id) {
          
          $query = $this->db->get_where('tb_product', array('store_id'=>$store_id));
          return $query->result_array();
      }
      /*
      function getFreeStudentListInOrganization($organization_id) {
          
          $student_ids = array();
          $students = $this->db->distinct()
                               ->select('t2.student_id')
                               ->from('tb_class as t1')
                               ->where('t1.organization_id', $organization_id)
                               ->join('tb_class_student as t2', 't1.id = t2.class_id')
                               ->get()
                               ->result_array();
          if (count($students) > 0) {
              foreach($students as $student) {
                  array_push($student_ids, $student['student_id']);      
              }
              
              return $this->db->select("*")
                       ->from('tb_user')
                       ->where_not_in('id', $student_ids)
                       ->where('user_type', 333)
                       ->get()
                       ->result_array();              
          } else {
              return $student_ids;
          }
      }
      */
      
      function getFreeStudentListInOrganization($organization_id) {
          
          $org_student_ids = array();
          $class_student_ids = array();
          
          $this->db->select('student_id');
          $this->db->from('tb_org_student');
          $this->db->where('organization_id', $organization_id);
          $org_studnets = $this->db->get()->result_array();
          
          if (count($org_studnets) > 0) {
              foreach($org_studnets as $org_studnet) {
                  array_push($org_student_ids, $org_studnet['student_id']);
              }                            
          }
          
          $this->db->select('student_id');
          $this->db->from('tb_class_student');
          $this->db->where('organization_id', $organization_id);
          $class_studnets = $this->db->get()->result_array();
          
          if (count($class_studnets) > 0) {
              foreach($class_studnets as $class_studnet) {
                  array_push($class_student_ids, $class_studnet['student_id']);
              }                          
          }
          
          if (count($class_student_ids) > 0) {
              
              foreach($class_student_ids as $class_std_id) {
              
                  if (($key = array_search($class_std_id, $org_student_ids, TRUE)) !== FALSE ) {
                      unset($org_student_ids[$key]);
                  }
              }              
          }
          
          if (count($org_student_ids) > 0 ) {
              
              return $this->db->select("*")
                       ->from('tb_user')
                       ->where_in('id', $org_student_ids)
                       ->where('user_type', 333)
                       ->get()
                       ->result_array();
          } else {
              return array();
          }
      }                
      
      
      function getFreeTeacherListInOrganization($organization_id) {
          
          $org_teacher_ids = array();
          $class_teacher_ids = array();
          
          $this->db->select('teacher_id');
          $this->db->from('tb_org_teacher');
          $this->db->where('organization_id', $organization_id);
          $org_teachers = $this->db->get()->result_array();
          
          if (count($org_teachers) > 0) {
              foreach($org_teachers as $org_teacher) {
                  array_push($org_teacher_ids, $org_teacher['teacher_id']);
              }                            
          }
          
          $this->db->select('teacher_id');
          $this->db->from('tb_class_teacher');
          $this->db->where('organization_id', $organization_id);
          $class_teachers = $this->db->get()->result_array();
          
          if (count($class_teachers) > 0) {
              foreach($class_teachers as $class_teacher) {
                  array_push($class_teacher_ids, $class_teacher['teacher_id']);
              }                          
          }
          
          if (count($class_teacher_ids) > 0) {
              
              foreach($class_teacher_ids as $class_teacher_id) {
              
                  if (($key = array_search($class_teacher_id, $org_teacher_ids, TRUE)) !== FALSE ) {
                      unset($org_teacher_ids[$key]);
                  }
              }              
          }
          
          if (count($org_teacher_ids) > 0 ) {
              
              return $this->db->select("*")
                       ->from('tb_user')
                       ->where_in('id', $org_teacher_ids)
                       ->where('user_type', 222)
                       ->get()
                       ->result_array();
          } else {
              return array();
          }
      }
      
      function getAllOrganizatedStudents($organization_id) {
          
          return $this->db->select('t1.*')
                      ->from('tb_user as t1')                       
                      ->join('tb_org_student as t2', 't1.id = t2.student_id')                       
                      ->where('t1.user_type', 333)
                      ->where('t2.organization_id', $organization_id)
                      ->get()
                      ->result_array();
      }
      
      function getAllUnOrganizatedStudents() { 
      
          $ids = array();         
          
          $ids_array = $this->db->select('t1.id as id')
                      ->from('tb_user as t1')                       
                      ->join('tb_org_student as t2', 't1.id = t2.student_id')                       
                      ->where('t1.user_type', 333)
                      //->where('t2.organization_id', $organization_id)
                      ->get()
                      ->result_array();
          
          if (count($ids_array) > 0) {
              foreach ($ids_array as $id) {
                  array_push($ids, $id['id']);              
              } 
                          
              return $this->db->select("*")
                       ->from('tb_user')
                       ->where_not_in('id', $ids)
                       ->where('user_type', 333)
                       ->get()
                       ->result_array();                
          } else {
              return array();
          }          
      }
      
      function removeTeacherFromOrg($org_id, $teacher_id) {
          
          $this->db->where('organization_id', $org_id);
          $this->db->where('teacher_id', $teacher_id);
          $this->db->delete('tb_org_teacher');
      }
      
      function removeStudentFromOrg($org_id, $teacher_id) {
          
          $this->db->where('organization_id', $org_id);
          $this->db->where('student_id', $teacher_id);
          $this->db->delete('tb_org_student');
      }
      
      function getStudentInClass($class_id) {
          
          return $this->db->select('tb_user.*')
                    ->from('tb_user')
                    ->join('tb_class_student', 'tb_user.id = tb_class_student.student_id')
                    ->where('tb_class_student.class_id', $class_id)
                    ->get()
                    ->result_array();
          
      }
      
      function getTeacherInClass($class_id) {
          
          return $this->db->select('tb_user.*')
                    ->from('tb_user')
                    ->join('tb_class_teacher', 'tb_user.id = tb_class_teacher.teacher_id')
                    ->where('tb_class_teacher.class_id', $class_id)
                    ->get()
                    ->result_array();
      }
      
      function removeTeacherInClass($class_id, $teacher_id) {
          
          $this->db->where('class_id', $class_id);
          $this->db->where('teacher_id', $teacher_id);
          $this->db->delete('tb_class_teacher');
      }
      
      function removeStudentInClass($class_id, $student_id) {
          
          $this->db->where('class_id', $class_id);
          $this->db->where('student_id', $student_id);
          $this->db->delete('tb_class_student');
      }
      
      function getTeacherClassList($teacher_id, $organization_id) {           
          
          return $this->db->select('t2.*')
                          ->from('tb_class_teacher as t1')
                          ->join('tb_class as t2', 't1.class_id = t2.id')
                          ->where('t1.organization_id', $organization_id)  
                          ->where('t1.teacher_id', $teacher_id)
                          ->get()
                          ->result_array();           
      }
      
      function getTeacherClassRanking($class_id) {
          
          return $this->db->select('tb_user.*, SUM(tb_statement.amount) as amount')
                    ->from('tb_class_teacher')
                    ->where('tb_class_teacher.class_id', $class_id)
                    ->join('tb_statement', 'tb_statement.to = tb_class_teacher.teacher_id')
                    ->join('tb_user', 'tb_user.id = tb_statement.to')
                    ->group_by('tb_statement.to')
                    ->order_by('amount', 'DESC')
                    ->get()
                    ->result_array(); 
      }
      
      function getStudentClassSpenderOne($class_id) {
          
          return $this->db->select('tb_user.*, SUM(tb_statement.amount) as amount')
                    ->from('tb_class_student')
                    ->where('tb_class_student.class_id', $class_id)
                    ->join('tb_statement', 'tb_statement.from = tb_class_student.student_id')
                    ->join('tb_user', 'tb_user.id = tb_statement.from')
                    ->group_by('tb_statement.from')
                    ->order_by('amount', 'DESC')
                    ->get()
                    ->result_array(); 
      }
      
      function getTeacherSpend($teacher_id, $organization_id) {
          
          return $this->db->select('amount, created_at as date')
                   ->from('tb_statement')
                   ->where('in', $organization_id)
                   ->where('from', $teacher_id)
                   ->get()
                   ->result_array();
      }
      
      function getTeacherEarned($teacher_id, $organization_id) {
          
          return $this->db->select('amount, created_at as date')
                   ->from('tb_statement')
                   ->where('in', $organization_id)
                   ->where('to', $teacher_id)
                   ->get()
                   ->result_array();
      }
      
      function getTeacherOrgId($teacher_id) {
          
          $query = $this->db->where('teacher_id', $teacher_id)
                        ->get('tb_org_teacher');
                        
          if ($query->num_rows() > 0) {
              return $query->row()->organization_id;
          } else {
              return false;
          }                
      }
      
      
  }
?>
