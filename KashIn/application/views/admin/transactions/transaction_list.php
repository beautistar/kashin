<!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css"> 
  

 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h3><i class="fa fa-list"></i> &nbsp; Transaction List</h3>
        </div>
        <!--<div class="col-md-6 text-right">
          <div class="btn-group margin-bottom-20"> 
            <a href="<?= base_url('admin/users/create_users_pdf'); ?>" class="btn btn-success">Export as PDF</a>
            <a href="<?= base_url('admin/users/export_csv'); ?>" class="btn btn-success">Export as CSV</a>
          </div>
        </div>-->
        
      </div>
    </div>
  </div>
   <div class="box border-top-solid">
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
          <th>No</th>
          <th>Raw File</th>
          <th>Finished File</th>
          <th>transaction ID</th>
          <th>Email</th>    
          <th>Status</th>
          <th>Timestamp(Opened)</th>
          <th>Timestamp Closed</th>
          <th>Editor ID</th>
          <th>Comments</th>
        </tr>
        </thead>
        <tbody>
          <?php $i=0; foreach($all_transactions as $row): $i++?>
          <tr>
            <td><?= $i;?></td>
            <td>
                <a href="<?= base_url('admin/transactions/download/'.$row['photo_id'].'/0'); ?>" class="btn btn-success btn-flat btn-xs"><i class="fa fa-download"></i> &nbsp; Download</a>
            </td>
            <td>
                <?php
                    if ($row['status'] == 1) { ?>
                        <a href="<?= base_url('admin/transactions/download/'.$row['photo_id'].'/1'); ?>" class="btn btn-success btn-flat btn-xs"><i class="fa fa-download"></i> &nbsp; Download</a>
            
                    <?php 
                    } else { ?>
                        <span class="btn btn-warning btn-flat btn-xs text-center">Pending<span> 
                    <?php 
                    }
                ?>
                
            </td>
            <td><?= $row['transaction_id']; ?></td>
            <td><?= $row['email']; ?></td>
            <td>
                <?php
                    if ($row['status'] == 0) { 
                ?>
                
                <a href="<?= base_url('admin/transactions/file_return/'.$row['id']); ?>" class="btn btn-info btn-flat btn-sm">Open</a>
                <?php 
                    } else {
                ?>
                <span class="btn btn-danger btn-flat btn-xs text-center">Closed<span>
                <?php
                }
                ?>
            </td>
            
            <td><?= $row['created_at']; ?></td>
            <td><?= $row['returned_at']=='0000-00-00 00:00:00'?'Not closed':$row['returned_at']; ?></td>
            <td><?= $row['editor_id']; ?></td>
            <td><?= $row['comment']; ?></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>  


<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>As you sure you want to delete.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script> 
  <script type="text/javascript">
      $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
  </script>
  
<script>
$("#view_users").addClass('active');
</script>   